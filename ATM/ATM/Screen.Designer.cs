namespace CashMachine
{
    partial class Screen
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Screen));
            this.promptLabel = new System.Windows.Forms.Label();
            this.CentralButton = new System.Windows.Forms.Button();
            this.hintImg = new System.Windows.Forms.PictureBox();
            this.CardNumberBox = new System.Windows.Forms.TextBox();
            this.AmountBox = new System.Windows.Forms.TextBox();
            this.PasswordBox = new System.Windows.Forms.TextBox();
            this.N20Label = new System.Windows.Forms.Label();
            this.N20Box = new System.Windows.Forms.TextBox();
            this.N50Box = new System.Windows.Forms.TextBox();
            this.N50Label = new System.Windows.Forms.Label();
            this.N200Box = new System.Windows.Forms.TextBox();
            this.N200Label = new System.Windows.Forms.Label();
            this.N100Box = new System.Windows.Forms.TextBox();
            this.N100Label = new System.Windows.Forms.Label();
            this.MaintButton = new System.Windows.Forms.Button();
            this.AmountLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.hintImg)).BeginInit();
            this.SuspendLayout();
            // 
            // promptLabel
            // 
            this.promptLabel.BackColor = System.Drawing.Color.Transparent;
            this.promptLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.promptLabel.ForeColor = System.Drawing.Color.Black;
            this.promptLabel.Location = new System.Drawing.Point(-5, 9);
            this.promptLabel.Name = "promptLabel";
            this.promptLabel.Size = new System.Drawing.Size(886, 78);
            this.promptLabel.TabIndex = 9;
            this.promptLabel.Text = "LabelText\r\nLabelText\r\n\r\n";
            this.promptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CentralButton
            // 
            this.CentralButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CentralButton.ForeColor = System.Drawing.Color.Black;
            this.CentralButton.Location = new System.Drawing.Point(351, 399);
            this.CentralButton.Name = "CentralButton";
            this.CentralButton.Size = new System.Drawing.Size(157, 39);
            this.CentralButton.TabIndex = 10;
            this.CentralButton.Text = "Insert card";
            this.CentralButton.UseVisualStyleBackColor = true;
            this.CentralButton.Click += new System.EventHandler(this.CentralButton_Click);
            // 
            // hintImg
            // 
            this.hintImg.BackColor = System.Drawing.Color.Transparent;
            this.hintImg.Image = global::CashMachine.Properties.Resources.insert_card;
            this.hintImg.Location = new System.Drawing.Point(303, 123);
            this.hintImg.Name = "hintImg";
            this.hintImg.Size = new System.Drawing.Size(251, 209);
            this.hintImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.hintImg.TabIndex = 11;
            this.hintImg.TabStop = false;
            this.hintImg.Visible = false;
            // 
            // CardNumberBox
            // 
            this.CardNumberBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CardNumberBox.ForeColor = System.Drawing.Color.Black;
            this.CardNumberBox.Location = new System.Drawing.Point(278, 123);
            this.CardNumberBox.MaxLength = 16;
            this.CardNumberBox.Name = "CardNumberBox";
            this.CardNumberBox.Size = new System.Drawing.Size(296, 44);
            this.CardNumberBox.TabIndex = 12;
            this.CardNumberBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CardNumberBox.TextChanged += new System.EventHandler(this.CardNumberBox_TextChanged);
            this.CardNumberBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // AmountBox
            // 
            this.AmountBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AmountBox.ForeColor = System.Drawing.Color.Black;
            this.AmountBox.Location = new System.Drawing.Point(351, 288);
            this.AmountBox.MaxLength = 5;
            this.AmountBox.Name = "AmountBox";
            this.AmountBox.Size = new System.Drawing.Size(157, 44);
            this.AmountBox.TabIndex = 13;
            this.AmountBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AmountBox.TextChanged += new System.EventHandler(this.AmountBox_TextChanged);
            this.AmountBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // PasswordBox
            // 
            this.PasswordBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PasswordBox.ForeColor = System.Drawing.Color.Black;
            this.PasswordBox.Location = new System.Drawing.Point(379, 154);
            this.PasswordBox.MaxLength = 4;
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.PasswordChar = '*';
            this.PasswordBox.Size = new System.Drawing.Size(100, 44);
            this.PasswordBox.TabIndex = 14;
            this.PasswordBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PasswordBox.TextChanged += new System.EventHandler(this.PasswordBox_TextChanged);
            this.PasswordBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // N20Label
            // 
            this.N20Label.AutoSize = true;
            this.N20Label.BackColor = System.Drawing.Color.Transparent;
            this.N20Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N20Label.ForeColor = System.Drawing.Color.Black;
            this.N20Label.Location = new System.Drawing.Point(25, 388);
            this.N20Label.Name = "N20Label";
            this.N20Label.Size = new System.Drawing.Size(59, 26);
            this.N20Label.TabIndex = 15;
            this.N20Label.Text = "20 x ";
            // 
            // N20Box
            // 
            this.N20Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N20Box.ForeColor = System.Drawing.Color.Black;
            this.N20Box.Location = new System.Drawing.Point(129, 388);
            this.N20Box.MaxLength = 2;
            this.N20Box.Name = "N20Box";
            this.N20Box.Size = new System.Drawing.Size(100, 32);
            this.N20Box.TabIndex = 0;
            this.N20Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // N50Box
            // 
            this.N50Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N50Box.ForeColor = System.Drawing.Color.Black;
            this.N50Box.Location = new System.Drawing.Point(129, 451);
            this.N50Box.MaxLength = 2;
            this.N50Box.Name = "N50Box";
            this.N50Box.Size = new System.Drawing.Size(100, 32);
            this.N50Box.TabIndex = 16;
            this.N50Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // N50Label
            // 
            this.N50Label.AutoSize = true;
            this.N50Label.BackColor = System.Drawing.Color.Transparent;
            this.N50Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N50Label.ForeColor = System.Drawing.Color.Black;
            this.N50Label.Location = new System.Drawing.Point(25, 454);
            this.N50Label.Name = "N50Label";
            this.N50Label.Size = new System.Drawing.Size(59, 26);
            this.N50Label.TabIndex = 17;
            this.N50Label.Text = "50 x ";
            // 
            // N200Box
            // 
            this.N200Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N200Box.ForeColor = System.Drawing.Color.Black;
            this.N200Box.Location = new System.Drawing.Point(735, 451);
            this.N200Box.MaxLength = 2;
            this.N200Box.Name = "N200Box";
            this.N200Box.Size = new System.Drawing.Size(100, 32);
            this.N200Box.TabIndex = 20;
            this.N200Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // N200Label
            // 
            this.N200Label.AutoSize = true;
            this.N200Label.BackColor = System.Drawing.Color.Transparent;
            this.N200Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N200Label.ForeColor = System.Drawing.Color.Black;
            this.N200Label.Location = new System.Drawing.Point(621, 451);
            this.N200Label.Name = "N200Label";
            this.N200Label.Size = new System.Drawing.Size(71, 26);
            this.N200Label.TabIndex = 21;
            this.N200Label.Text = "200 x ";
            // 
            // N100Box
            // 
            this.N100Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N100Box.ForeColor = System.Drawing.Color.Black;
            this.N100Box.Location = new System.Drawing.Point(735, 385);
            this.N100Box.MaxLength = 2;
            this.N100Box.Name = "N100Box";
            this.N100Box.Size = new System.Drawing.Size(100, 32);
            this.N100Box.TabIndex = 18;
            this.N100Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // N100Label
            // 
            this.N100Label.AutoSize = true;
            this.N100Label.BackColor = System.Drawing.Color.Transparent;
            this.N100Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N100Label.ForeColor = System.Drawing.Color.Black;
            this.N100Label.Location = new System.Drawing.Point(621, 385);
            this.N100Label.Name = "N100Label";
            this.N100Label.Size = new System.Drawing.Size(71, 26);
            this.N100Label.TabIndex = 19;
            this.N100Label.Text = "100 x ";
            // 
            // MaintButton
            // 
            this.MaintButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaintButton.ForeColor = System.Drawing.Color.Black;
            this.MaintButton.Location = new System.Drawing.Point(351, 444);
            this.MaintButton.Name = "MaintButton";
            this.MaintButton.Size = new System.Drawing.Size(157, 39);
            this.MaintButton.TabIndex = 22;
            this.MaintButton.Text = "Maintenance";
            this.MaintButton.UseVisualStyleBackColor = true;
            this.MaintButton.Click += new System.EventHandler(this.MaintButton_Click);
            // 
            // AmountLabel
            // 
            this.AmountLabel.BackColor = System.Drawing.Color.Transparent;
            this.AmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AmountLabel.ForeColor = System.Drawing.Color.Black;
            this.AmountLabel.Location = new System.Drawing.Point(-5, 187);
            this.AmountLabel.Name = "AmountLabel";
            this.AmountLabel.Size = new System.Drawing.Size(886, 78);
            this.AmountLabel.TabIndex = 23;
            this.AmountLabel.Text = "LabelText\r\nLabelText\r\n\r\n";
            this.AmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = global::CashMachine.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(893, 498);
            this.Controls.Add(this.AmountLabel);
            this.Controls.Add(this.MaintButton);
            this.Controls.Add(this.N200Box);
            this.Controls.Add(this.N200Label);
            this.Controls.Add(this.N100Box);
            this.Controls.Add(this.N100Label);
            this.Controls.Add(this.N50Box);
            this.Controls.Add(this.N50Label);
            this.Controls.Add(this.N20Box);
            this.Controls.Add(this.N20Label);
            this.Controls.Add(this.PasswordBox);
            this.Controls.Add(this.AmountBox);
            this.Controls.Add(this.CardNumberBox);
            this.Controls.Add(this.hintImg);
            this.Controls.Add(this.CentralButton);
            this.Controls.Add(this.promptLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Screen";
            this.Text = "ATM";
            ((System.ComponentModel.ISupportInitialize)(this.hintImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label promptLabel;
        private System.Windows.Forms.Button CentralButton;
        private System.Windows.Forms.PictureBox hintImg;
        private System.Windows.Forms.TextBox CardNumberBox;
        private System.Windows.Forms.TextBox AmountBox;
        private System.Windows.Forms.TextBox PasswordBox;
        private System.Windows.Forms.Label N20Label;
        private System.Windows.Forms.TextBox N20Box;
        private System.Windows.Forms.TextBox N50Box;
        private System.Windows.Forms.Label N50Label;
        private System.Windows.Forms.TextBox N200Box;
        private System.Windows.Forms.Label N200Label;
        private System.Windows.Forms.TextBox N100Box;
        private System.Windows.Forms.Label N100Label;
        private System.Windows.Forms.Button MaintButton;
        private System.Windows.Forms.Label AmountLabel;
    }
}