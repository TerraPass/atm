﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CashMachine
{
    public partial class Screen : Form
    {
        // An ATM instance this specific form provides an interface for.
        private ATM _simAtm;
        const int BTN_COUNT = 8;
        private Button[] physButtons = new Button[BTN_COUNT];
        public Screen(ATM simulatedAtm)
        {
            InitializeComponent();
            _simAtm = simulatedAtm;
            _simAtm.screen = this;
            for (int i = 0; i < BTN_COUNT; i++)
            {
                physButtons[i] = new Button();
                physButtons[i].Size = new Size(208, 61);
                if (i < 4)
                    physButtons[i].Location = new Point(10, 101 + i * 80);
                else
                    physButtons[i].Location = new Point(Size.Width - 223, 101 + (i - 4) * 80);
                physButtons[i].Visible = physButtons[i].Enabled = false;
                physButtons[i].BackColor = System.Drawing.Color.White;
                physButtons[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                physButtons[i].Tag = new List<string>();
                Controls.Add(physButtons[i]);
            }
            ((List<string>)(physButtons[1].Tag)).Add("transferButton");
            ((List<string>)(physButtons[2].Tag)).Add("exitButton");
            ((List<string>)(physButtons[3].Tag)).Add("exitButton2");
            ((List<string>)(physButtons[5].Tag)).Add("ledgerButton");
            ((List<string>)(physButtons[6].Tag)).Add("withdrawalButton");
            ((List<string>)(physButtons[6].Tag)).Add("proceedButton");
            ((List<string>)(physButtons[7].Tag)).Add("backButton");
            physButtons[0].Click += OnPhysButton0_Click;
            physButtons[1].Click += OnPhysButton1_Click;
            physButtons[2].Click += OnPhysButton2_Click;
            physButtons[3].Click += OnPhysButton3_Click;
            physButtons[4].Click += OnPhysButton4_Click;
            physButtons[5].Click += OnPhysButton5_Click;
            physButtons[6].Click += OnPhysButton6_Click;
            physButtons[7].Click += OnPhysButton7_Click;
            DrawMaintenance();
        }

        private Button FindByTag(string tag)
        {
            Button res = null;
            for (int i = 0; i < BTN_COUNT; i++)
            {
                List<string> list = (List<string>)(physButtons[i].Tag);
                list.Sort();
                if (list.BinarySearch(tag) >= 0)
                    res = physButtons[i];
            }
            return res;
        }

        public void DrawMaintenance()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Maintenance mode. Add some money and start ATM";

            hintImg.Visible = true;
            hintImg.Image = Properties.Resources.sad_computer_md;

            CentralButton.Visible = true;
            CentralButton.Text = "Start ATM";

            uint[] nom = _simAtm.nominals;
            N20Label.Visible = N20Box.Visible = true;
            N20Label.Text = nom[0].ToString() + ": " + _simAtm.moneyAvailable[nom[0]].ToString() + "/" + _simAtm.limitNotes;
            N50Label.Visible = N50Box.Visible = true;
            N50Label.Text = nom[1].ToString() + ": " + _simAtm.moneyAvailable[nom[1]].ToString() + "/" + _simAtm.limitNotes;
            N100Label.Visible = N100Box.Visible = true;
            N100Label.Text = nom[2].ToString() + ": " + _simAtm.moneyAvailable[nom[2]].ToString() + "/" + _simAtm.limitNotes;
            N200Label.Visible = N200Box.Visible = true;
            N200Label.Text = nom[3].ToString() + ": " + _simAtm.moneyAvailable[nom[3]].ToString() + "/" + _simAtm.limitNotes;
        }

        public void DrawIdle()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Please, insert your card";

            hintImg.Visible = true;
            hintImg.Image = Properties.Resources.insert_card;

            CentralButton.Visible = true;
            CentralButton.Text = "Insert card";

            MaintButton.Visible = true;
        }

        public void DrawLoading()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Waiting...";

            hintImg.Visible = true;
            hintImg.Image = Properties.Resources.hourglass;
        }

        public void DrawFaceControlFailed(string verdict)
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = (verdict == "INVALID" ? "Invalid card number" : "This card is blocked");

            hintImg.Visible = true;
            hintImg.Image = (verdict == "INVALID" ? Properties.Resources.invalid_card : Properties.Resources.card_blocked);

            CentralButton.Visible = true;
            CentralButton.Text = (verdict == "INVALID" ? "Eject card" : "Walk away");
        }

        public void DrawRequestPIN()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Please, enter your PIN-code";

            PasswordBox.Visible = true;

            Button exitButton = FindByTag("exitButton");
            exitButton.Visible = exitButton.Enabled = true;
            exitButton.Text = "Exit";

            Button proceedButton = FindByTag("proceedButton");
            proceedButton.Visible = true;
            proceedButton.Enabled = false;
            proceedButton.Text = "Proceed";
        }

        public void DrawMainMenu()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Choose an action";

            Button transferButton = FindByTag("transferButton"),
                exitButton = FindByTag("exitButton"), ledgerButton = FindByTag("ledgerButton"),
                withdrawalButton = FindByTag("withdrawalButton");
            transferButton.Visible = transferButton.Enabled = true;
            transferButton.Text = "Transfer";
            exitButton.Visible = exitButton.Enabled = true;
            exitButton.Text = "Exit";
            ledgerButton.Visible = ledgerButton.Enabled = true;
            ledgerButton.Text = "Ledger";
            withdrawalButton.Visible = withdrawalButton.Enabled = true;
            withdrawalButton.Text = "Withdraw";
        }

        public void DrawWithdrawal()
        {
            HideEverything();

            promptLabel.Visible = true;
            promptLabel.Text = "Choose an amount to withdraw";

            for (int i = 0; i < BTN_COUNT; i++)
            {
                physButtons[i].Visible = physButtons[i].Enabled = true;
                if (i < 3) physButtons[i].Text = _simAtm.defaultAmounts[i].ToString();
            }
            physButtons[4].Text = _simAtm.defaultAmounts[3].ToString();
            physButtons[5].Text = _simAtm.defaultAmounts[4].ToString();
            physButtons[6].Text = "Other sum";

            Button exitButton = FindByTag("exitButton2"), backButton = FindByTag("backButton");
            exitButton.Text = "Exit";
            backButton.Text = "Main menu";
        }

        public void DrawResultWithdrawal(List<uint> data)
        {
            HideEverything();

            CentralButton.Visible = true;
            CentralButton.Text = "Main menu";

            promptLabel.Visible = true;
            switch (data[0])
            {
                case ATM.WITHDRAWAL_ZERO:
                    promptLabel.Text = "No money? No honey!";
                break;

                case ATM.WITHDRAWAL_NOT_MULT_TEN:
                    promptLabel.Text = "This sum is not a multiple of 10";
                break;

                case ATM.WITHDRAWAL_BALANCE_LOW:
                    promptLabel.Text = "Not enough money on your available balance";
                break;

                case ATM.WITHDRAWAL_IMPOSSIBLE:
                    promptLabel.Text = "Impossible to withdraw this sum with the available bank notes.\nOther possible sums:";
                    promptLabel.Text += data[1].ToString();
                    if (data.ToArray().Length > 2)
                        promptLabel.Text += data[2].ToString();
                break;

                case ATM.WITHDRAWAL_OK:
                    promptLabel.Text = data[1].ToString() + " has been withdrawn: ";
                    int size = data.ToArray().Length;
                    for (int i = 2; i < size; i += 2)
                    {
                        promptLabel.Text += data[i].ToString() + "x" + data[i + 1].ToString();
                        if (i + 2 < size)
                            promptLabel.Text += " + ";
                    }
                break;
                default: throw new NotImplementedException();

            }
        }

        public void DrawTransferResult(int res)
        {
            HideEverything();

            CentralButton.Visible = true;
            CentralButton.Text = "Proceed";

            promptLabel.Visible = true;
            switch (res)
            {
                case ATM.TRANSFER_LOW_BALANCE:
                    promptLabel.Text = "Not enough funds available";
                    break;

                case ATM.TRANSFER_INVALID_RECIPIENT:
                    promptLabel.Text = "Recipient account number is invalid";
                    break;

                case ATM.TRANSFER_OK:
                    promptLabel.Text = "Transfer succeeded";
                    break;               
                default: throw new NotImplementedException();

            }
        }

        public void DrawLedgerMenu()
        {//1234567890123456
            HideEverything();
            promptLabel.Visible = true;
            string bal = _simAtm.AvailableBalance().ToString();
            if (bal.Contains('.'))
                bal = bal.Substring(0, bal.IndexOf('.') + 3);
            promptLabel.Text = "Your current balance: " + _simAtm.currentCard.balance +
                "\nYour available balance: " + bal;
            Button backButton = FindByTag("backButton"), exitButton = FindByTag("exitButton2");
            exitButton.Visible = exitButton.Enabled = true;
            exitButton.Text = "Exit";
            //exitButton.BackColor = Color.White;
            backButton.Visible = backButton.Enabled = true;
            backButton.Text = "Back";
            //backButton.BackColor = Color.White;
        }

        public void DrawTransferMenu()
        {
            HideEverything();
            AmountBox.Visible = CardNumberBox.Visible = true;
            AmountBox.Enabled = CardNumberBox.Enabled = true;
            Button backButton = FindByTag("backButton"), exitButton = FindByTag("exitButton2"),
                proceedButton = FindByTag("proceedButton");
            exitButton.Visible = exitButton.Enabled = true;
            exitButton.Text = "Exit";
            backButton.Visible = backButton.Enabled = true;
            backButton.Text = "Back";
            proceedButton.Visible = proceedButton.Enabled = true;
            proceedButton.Text = "Proceed";
            promptLabel.Visible = true;
            promptLabel.Text = "Please enter the card number";
            AmountLabel.Visible = true;
            AmountLabel.Text = "Please enter the sum of money";
        }

        private void HideEverything()
        {
            for (int i = 0; i < BTN_COUNT; i++)
                physButtons[i].Visible = physButtons[i].Enabled = false;
            promptLabel.Visible = false;
            CentralButton.Visible = false;
            MaintButton.Visible = false;
            hintImg.Visible = false;
            CardNumberBox.Visible = false; CardNumberBox.Text = "";
            AmountBox.Visible = false; AmountBox.Text = "";
            PasswordBox.Visible = false; PasswordBox.Text = "";
            N20Label.Visible = N20Box.Visible = false; N20Box.Text = "";
            N50Label.Visible = N50Box.Visible = false; N50Box.Text = "";
            N100Label.Visible = N100Box.Visible = false; N100Box.Text = "";
            N200Label.Visible = N200Box.Visible = false; N200Box.Text = "";
            AmountLabel.Visible = false;
        }

        private void CentralButton_Click(object sender, EventArgs e)
        {
            switch (CentralButton.Text)
            {
                case "Start ATM":
                    uint sum;
                    uint.TryParse(N20Box.Text, out sum);
                    _simAtm.AddMoney(20, sum);
                    uint.TryParse(N50Box.Text, out sum);
                    _simAtm.AddMoney(50, sum);
                    uint.TryParse(N100Box.Text, out sum);
                    _simAtm.AddMoney(100, sum);
                    uint.TryParse(N200Box.Text, out sum);
                    _simAtm.AddMoney(200, sum);
                    _simAtm.AcceptInput(ATM.InputType.OK);
                    _simAtm.ReturnToIdle();
                break;
                
                case "Insert card":
                    CardForm form = new CardForm(_simAtm);
                    form.ShowDialog();
                break;

                case "Eject card":
                    _simAtm.ReturnToIdle();
                break;

                case "Main menu":
                    _simAtm.MainMenu();
                break;

                case "Proceed":
                    _simAtm.MainMenu();
                break;
                
                case "Walk away":
                    _simAtm.ReturnToIdle();
                break;
            }
        }

        private void PasswordBox_TextChanged(object sender, EventArgs e)
        {
            Button proceedButton = FindByTag("proceedButton");
            proceedButton.Enabled = (PasswordBox.Text.Trim().Length == 4);
        }

        private void MaintButton_Click(object sender, EventArgs e)
        {
            CardForm form = new CardForm(_simAtm, "Enter the password to go to maintenance mode", 16);
            form.ShowDialog();
        }

        private void OnPhysButton0_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.MenuWithdrawal:
                    _simAtm.CheckWithdraw(_simAtm.defaultAmounts[0]);
                break;
            }
        }

        private void OnPhysButton1_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.MenuWithdrawal:
                    _simAtm.CheckWithdraw(_simAtm.defaultAmounts[1]);
                break;
                case ATM.State.MenuMain:
                    _simAtm.TransferMenu();
                break;
            }
        }

        private void OnPhysButton2_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.RequestPIN:
                case ATM.State.MenuMain:
                    _simAtm.EjectCard();
                break;

                case ATM.State.MenuWithdrawal:
                    _simAtm.CheckWithdraw(_simAtm.defaultAmounts[2]);
                break;
            }
        }

        private void OnPhysButton3_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            { // "Exit" pressed
                case ATM.State.MenuWithdrawal:
                case ATM.State.MenuLedger:
                case ATM.State.MenuTransfer:
                    _simAtm.EjectCard();
                break;
            }
        }

        private void OnPhysButton4_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.MenuWithdrawal:
                    _simAtm.CheckWithdraw(_simAtm.defaultAmounts[3]);
                break;
            }
        }

        private void OnPhysButton5_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.MenuMain:
                    _simAtm.LedgerMenu();
                break;

                case ATM.State.MenuWithdrawal:
                    _simAtm.CheckWithdraw(_simAtm.defaultAmounts[4]);
                break;
            }
        }

        private void OnPhysButton6_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            {
                case ATM.State.RequestPIN:
                    string password = PasswordBox.Text;
                    PasswordBox.Text = "";
                    if (_simAtm.CheckPIN(password))
                    {
                        _simAtm.MainMenu();
                    }
                    else
                    {
                        if (_simAtm.attemptsPIN == _simAtm.attemptsAllowedPIN)
                        {
                            MessageBox.Show("Wrong PIN-code. No attempts left. Your card is blocked.",
                                "Card is blocked", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _simAtm.BlockCard();
                        }
                        else
                        {
                            MessageBox.Show("Wrong PIN-code. " + (_simAtm.attemptsAllowedPIN - _simAtm.attemptsPIN).ToString() + " attempts left.",
                                "Wrong password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                break;

                case ATM.State.MenuMain:
                    _simAtm.WithdrawalMenu();
                break;

                case ATM.State.MenuWithdrawal:
                    CardForm form = new CardForm(_simAtm, "Input the amount that is a multiple of 10", 4);
                    form.ShowDialog();
                break;
                case ATM.State.MenuTransfer:
                    //try
                    //{
                    if (CardNumberBox.Text.Count() > 0 && AmountBox.Text.Count() > 0)
                        _simAtm.CheckTransfer(CardNumberBox.Text, float.Parse(AmountBox.Text));
                    //}
                    //catch (FormatException fe)
                    //{
                    //    DrawTransferResult(1);
                    //}
                break;
                default:
                break;
            }
        }

        private void OnPhysButton7_Click(object sender, EventArgs e)
        {
            switch (_simAtm.currentState)
            { // "Back" pressed
                case ATM.State.MenuWithdrawal:
                case ATM.State.MenuLedger:
                case ATM.State.MenuTransfer:
                    _simAtm.MainMenu();
                break;
            }
        }

        private void OnlyDigits(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && 
                !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void AmountBox_TextChanged(object sender, EventArgs e)
        {
            if (_simAtm.currentState == ATM.State.MenuTransfer)
            {
                //ulong ul;
                //uint ui;
                physButtons[7].Enabled = /*ulong.TryParse(CardNumberBox.Text, out ul) && */(CardNumberBox.Text.Trim().Length == 16) &&
                    /*uint.TryParse(AmountBox.Text, out ui) && */(AmountBox.Text.Trim().Length > 0);
            }
        }

        private void CardNumberBox_TextChanged(object sender, EventArgs e)
        {
            if (_simAtm.currentState == ATM.State.MenuTransfer)
            {
                //ulong ul;
                //uint ui;
                physButtons[7].Enabled = /*ulong.TryParse(CardNumberBox.Text, out ul) && */(CardNumberBox.Text.Trim().Length == 16) &&
                    /*uint.TryParse(AmountBox.Text, out ui) && */(AmountBox.Text.Trim().Length > 0);
            }
        }

    }
}