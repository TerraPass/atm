﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public interface IRecipient
    {
        void HandleMessage(Message msg);
    }
}
