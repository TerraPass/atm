﻿namespace CashMachine
{
    partial class CardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardForm));
            this.HintLabel = new System.Windows.Forms.Label();
            this.NumBox = new System.Windows.Forms.TextBox();
            this.ProceedButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HintLabel
            // 
            this.HintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HintLabel.Location = new System.Drawing.Point(26, 9);
            this.HintLabel.Name = "HintLabel";
            this.HintLabel.Size = new System.Drawing.Size(252, 57);
            this.HintLabel.TabIndex = 0;
            this.HintLabel.Text = "Enter your card number";
            // 
            // NumBox
            // 
            this.NumBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.NumBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumBox.Location = new System.Drawing.Point(26, 92);
            this.NumBox.MaxLength = 16;
            this.NumBox.Name = "NumBox";
            this.NumBox.Size = new System.Drawing.Size(252, 38);
            this.NumBox.TabIndex = 1;
            this.NumBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumBox.TextChanged += new System.EventHandler(this.NumBox_TextChanged);
            if (this.HintLabel.Text != "Enter the password to go to maintenance mode")
                this.NumBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyDigits);
            // 
            // ProceedButton
            // 
            this.ProceedButton.Enabled = false;
            this.ProceedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ProceedButton.Location = new System.Drawing.Point(59, 151);
            this.ProceedButton.Name = "ProceedButton";
            this.ProceedButton.Size = new System.Drawing.Size(190, 48);
            this.ProceedButton.TabIndex = 2;
            this.ProceedButton.Text = "Proceed";
            this.ProceedButton.UseVisualStyleBackColor = true;
            this.ProceedButton.Click += new System.EventHandler(this.ProceedButton_Click);
            // 
            // CardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 211);
            this.Controls.Add(this.ProceedButton);
            this.Controls.Add(this.NumBox);
            this.Controls.Add(this.HintLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CardForm";
            this.Text = "Card number";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CardFrom_Closing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HintLabel;
        private System.Windows.Forms.TextBox NumBox;
        private System.Windows.Forms.Button ProceedButton;
    }
}