﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class ATM_Maintenance : State<ATM>
    {
        private static ATM_Maintenance _instance;
        private ATM_Maintenance()
        { }

        public static ATM_Maintenance instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Maintenance();
                return _instance;
            }
        }

        public override void OnEnter(ATM owner)
        {
            MessageDispatcher.instance.DispatchMessage(owner, Bank.instance, Message.Type.NO_MONEY, "");
        }
        public override void Execute(ATM owner)
        {
            
        }
        public override void OnExit(ATM owner)
        {
            
        }
        public override void OnMessage(ATM owner, Message msg)
        {
            if (msg.messageType == Message.Type.INPUT)
            {
                if (msg.content == ATM.InputType.OK.ToString())
                {
                    owner.stateMachine.ChangeState(ATM_Idle.instance);
                }
            }
        }
    }

    public class ATM_Idle : State<ATM>
    {
        private static ATM_Idle _instance;
        private ATM_Idle()
        { }

        public static ATM_Idle instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Idle();
                return _instance;
            }
        }

        public override void OnEnter(ATM owner)
        {
            if (!owner.HasMoney())
                owner.stateMachine.ChangeState(ATM_Maintenance.instance);
        }
        public override void Execute(ATM owner)
        {
            if (owner.currentCard != null)
                owner.stateMachine.ChangeState(ATM_CardInserted.instance);
        }
        public override void OnExit(ATM owner)
        {
            
        }
        public override void OnMessage(ATM owner, Message msg)
        {
            //if (msg.messageType == Message.Type.INPUT)
            //{
            //    if (msg.content.Substring(0,msg.content.IndexOf(';')) == ATM.InputType.OK.ToString())
            //    {
            //        owner.stateMachine.ChangeState(ATM_CardInserted.instance);
            //    }
            //}
        }
    }

    public class ATM_CardInserted : State<ATM>
    {
        private static ATM_CardInserted _instance;
        private ATM_CardInserted()
        { }

        public static ATM_CardInserted instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_CardInserted();
                return _instance;
            }
        }

        public override void OnEnter(ATM owner)
        {
            MessageDispatcher.instance.DispatchMessage(owner, Bank.instance, Message.Type.VALIDATE_CARD, owner.currentCard.cardNumber);
        }
        public override void Execute(ATM owner)
        {
            //TODO
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {
            if (msg.messageType == Message.Type.VALIDATE_CARD)
            {
                if (msg.content == "INVALID" || msg.content == "BLOCKED")
                {
                    if (msg.content == "INVALID")
                        owner.EjectCard();
                    owner.stateMachine.ChangeState(ATM_Idle.instance);
                }
                else
                {
                    //1234;0.00;0;0;0.00;0.00
                    // "PIN;credit_limit;commission_own;comission_credit;min_comission_own;min_comission_credit"
                    string cont = msg.content;
                    owner.currentCard.expectedPIN = cont.Substring(0,cont.IndexOf(';'));
                    cont = cont.Remove(0, cont.IndexOf(';') + 1);
                    float f;
                    float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                    owner.currentCard.creditLimit = f;
                    cont = cont.Remove(0, cont.IndexOf(';'));
                    float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                    owner.currentCard.comissionOwn = f;
                    cont = cont.Remove(0, cont.IndexOf(';'));
                    float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                    owner.currentCard.comissionCredit = f;
                    cont = cont.Remove(0, cont.IndexOf(';'));
                    float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                    owner.currentCard.minComissionOwn = f;
                    cont = cont.Remove(0, cont.IndexOf(';'));
                    float.TryParse(cont, out f);
                    owner.currentCard.minComissionCredit = f;
                    owner.stateMachine.ChangeState(ATM_RequestPIN.instance);
                }
            }
        }
    }

    public class ATM_RequestPIN : State<ATM>
    {
        private static ATM_RequestPIN _instance;
        private ATM_RequestPIN() 
        { }

        public static ATM_RequestPIN instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_RequestPIN();
                return _instance;
            }
        }

        public override void OnEnter(ATM owner)
        {
            if(owner.screen != null)
                owner.screen.RefreshLayout();
        }
        public override void Execute(ATM owner)
        {
            // TODO or not to do
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {
            if (msg.messageType == Message.Type.INPUT)
            {
                if (msg.content.Substring(0, msg.content.IndexOf(':')) == ATM.InputType.OK.ToString())
                {
                    string enteredPIN = msg.content.Substring(msg.content.IndexOf(':')+1);
                    if (owner.CheckPIN(enteredPIN))
                        owner.stateMachine.ChangeState(ATM_Menu_Main.instance);
                    else
                    {
                        owner.stateMachine.ChangeState(ATM_RequestPIN.instance);
                    }
                }
            }
        }
    }

    public abstract class ATM_Menu : State<ATM>
    {        
        
    }

    public class ATM_Menu_Main : ATM_Menu
    {
        private ATM_Menu_Main() 
        { }
        private static ATM_Menu_Main _instance;
        public static ATM_Menu_Main instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Menu_Main();
                return _instance;
            }
        }
        public override void OnEnter(ATM owner)
        {
            //TODO
        }
        public override void Execute(ATM owner)
        {
            //TODO
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {

        }
    }

    public class ATM_Menu_Ledger : ATM_Menu
    {
        private ATM_Menu_Ledger() 
        { }
        private static ATM_Menu_Ledger _instance;
        public static ATM_Menu_Ledger instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Menu_Ledger();
                return _instance;
            }
        }
        public override void OnEnter(ATM owner)
        {
            MessageDispatcher.instance.DispatchMessage(owner, Bank.instance, Message.Type.BALANCE, owner.currentCard.cardNumber);
        }
        public override void Execute(ATM owner)
        {
            //TODO
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {
            if (msg.messageType == Message.Type.BALANCE)
            {
                owner.currentCard.balance = int.Parse(msg.content);
            }
        }
    }

    public class ATM_Menu_Withdrawal : ATM_Menu
    {
        private ATM_Menu_Withdrawal() 
        { }
        private static ATM_Menu_Withdrawal _instance;
        public static ATM_Menu_Withdrawal instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Menu_Withdrawal();
                return _instance;
            }
        }
        public override void OnEnter(ATM owner)
        {
            //TODO
        }
        public override void Execute(ATM owner)
        {
            //TODO
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {

        }
    }

    public class ATM_Menu_Transfer : ATM_Menu
    {
        private ATM_Menu_Transfer() 
        { }
        private static ATM_Menu_Transfer _instance;
        public static ATM_Menu_Transfer instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ATM_Menu_Transfer();
                return _instance;
            }
        }
        public override void OnEnter(ATM owner)
        {
            //TODO
        }
        public override void Execute(ATM owner)
        {
            //TODO
        }
        public override void OnExit(ATM owner)
        {
            //TODO
        }
        public override void OnMessage(ATM owner, Message msg)
        {

        }
    }

    //public class ATM_Menu_RegularTransfer : ATM_Menu
    //{
    //    private ATM_Menu_RegularTransfer() 
    //    { }
    //    private static ATM_Menu_RegularTransfer _instance;
    //    public static ATM_Menu_RegularTransfer instance
    //    {
    //        get
    //        {
    //            if (_instance == null)
    //                _instance = new ATM_Menu_RegularTransfer();
    //            return _instance;
    //        }
    //    }
    //    public override void OnEnter(ATM owner)
    //    {
    //        //TODO
    //    }
    //    public override void Execute(ATM owner)
    //    {
    //        //TODO
    //    }
    //    public override void OnExit(ATM owner)
    //    {
    //        //TODO
    //    }
    //    public override void OnMessage(ATM owner, Message msg)
    //    {

    //    }
    //}
}