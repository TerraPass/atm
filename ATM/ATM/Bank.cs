﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class Bank : IRecipient
    {
        //private StateMachine<Bank> _sm;

        private const string BANK_PASSKEY = "666";
        private const string BANK_SERVER_URL = "http://localserver/cashmachine/";//"http://terrapass.kodingen.com/cashmachine/";
        private const string BANK_SERVER_CARDINFO = "cardinfo.php";
        private const string BANK_SERVER_BLOCK = "block.php";
        private const string BANK_SERVER_BALANCE = "balance.php";
        private const string BANK_SERVER_WITHDRAW = "withdraw.php";
        private const string BANK_SERVER_TRANSFER = "transfer.php";

        private const uint BANK_SERVER_CONNECTION_RETRIES = 3;

        public const string BANK_CONNECTION_ERROR = "ERROR";
        public const string BANK_TRANSACTION_ERROR = "FAIL";        

        private static Bank _instance;
        private Bank()
        {
            
        }

        public static Bank instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Bank();
                return _instance;
            }
        }

        public bool CheckPassword(string password)
        {
            return BANK_PASSKEY == password;
        }

        public void HandleMessage(Message msg)
        {
            string responseContent = "";
            //try
            //{
                switch (msg.messageType)
                {
                    case Message.Type.VALIDATE_CARD:
                        responseContent = ValidateCard(msg.content).ToString();
                        break;
                    case Message.Type.BALANCE:
                        responseContent = GetBalance(msg.content).ToString();
                        break;
                    case Message.Type.NO_MONEY:
                    
                        break;
                    case Message.Type.WITHDRAWAL:
                        responseContent = WithdrawMoney(msg.content.Substring(0,msg.content.IndexOf(';'))
                                                        , uint.Parse(msg.content.Substring(msg.content.IndexOf(';') + 1)));  //withdrawStr as "1234567890123456;100.00"
                        break;
                    case Message.Type.BLOCK_CARD:
                        responseContent = BlockCard(msg.content);
                        break;
                    case Message.Type.TRANSFER:
                        string cardNum = msg.content.Substring(0,msg.content.IndexOf(';'));
                        string destCardNum = msg.content.Substring(msg.content.IndexOf(';') + 1, msg.content.LastIndexOf(';') - (msg.content.IndexOf(';') + 1));
                        float amount = float.Parse(msg.content.Substring(msg.content.LastIndexOf(';')+1));
                        responseContent = TransferMoney(cardNum, destCardNum, amount);
                        break;
                    default: throw new NotImplementedException();                
                }
            //}
            //catch(Exception e)
            //{
                 
            //}
                MessageDispatcher.instance.DispatchMessage(this,msg.sender,msg.messageType,responseContent);
        }

        private string ConnectToDB(string url, string param)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            wc.Headers.Set("Content-type", "application/x-www-form-urlencoded");
            uint retries = 0;
            string res = BANK_CONNECTION_ERROR;
            while (retries < BANK_SERVER_CONNECTION_RETRIES)
            {
                try
                {
                    res = wc.UploadString(BANK_SERVER_URL + url, "passkey=" + BANK_PASSKEY + '&' + param);
                    break;
                }
                catch (System.Net.WebException)
                {
                    retries++;
                    continue;
                }
            }
            return res;
        }

        /// <summary>
        /// Connects to the bank DB and gets data about the card.
        /// </summary>
        /// <param name="cardNum"></param>
        /// <returns></returns>
        private string ValidateCard(string cardNum)
        {
            return ConnectToDB(BANK_SERVER_CARDINFO, "cardnum=" + cardNum);
        }

        private string BlockCard(string cardNum)
        {
            return ConnectToDB(BANK_SERVER_BLOCK, "cardnum=" + cardNum);          
        }

        private string GetBalance(string cardNum)
        {
            string balanceStr = ConnectToDB(BANK_SERVER_BALANCE, "cardnum=" + cardNum);
            //if (balanceStr != "INVALID")
            //{
              //  balanceStr[balanceStr.IndexOf('.')]
            //}
            return balanceStr;
        }

        private string WithdrawMoney(string cardNum, uint amnt)
        {
            return ConnectToDB(BANK_SERVER_WITHDRAW, "cardnum=" + cardNum + "&ammount=" + amnt);                        
        }

        private string TransferMoney(string cardNum, string destCardNum, double amnt)
        {
            return ConnectToDB(BANK_SERVER_TRANSFER, "cardnum=" + cardNum + "&destcardnum=" + destCardNum + "&ammount=" + amnt);
        }
    }
}
