﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{    
    public class Bank_Idle : State<Bank>
    {
        private static Bank_Idle _instance;
        private Bank_Idle()
        {
            //TODO
        }

        public static Bank_Idle instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Bank_Idle();
                return _instance;
            }
        }

        public override void OnEnter(Bank owner)
        {
            //TODO
        }
        public override void Execute(Bank owner)
        {
            //TODO
        }
        public override void OnExit(Bank owner)
        {
            //TODO
        }

        public override void OnMessage(Bank owner, Message msg)
        {
            //TODO
        }
    }    
}