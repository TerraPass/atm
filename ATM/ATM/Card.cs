﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class Card
    {
        public string cardNumber = "";
        public string expectedPIN = null;
        private float _lastKnownBalance = 0;
        
        public float creditLimit = 0; // скільки можна "зайняти" в банка
        public float comissionOwn = 0; // скільки % бере банк при знятті
        public float comissionCredit = 0; // скільки % комісії бере банк за "свої" гроші (позика)
        public float minComissionOwn = 0; // скільки грн (мін) бере банк при знятті
        public float minComissionCredit = 0; // скільки грн (мін) хоче взяти банк (позика)

        public Card(string cardNum)
        {
            cardNumber = cardNum;
        }

        public void ReduceBalance(int amount)
        {
            balance -= amount; //CORRECT THIS
        }

        public float balance
        {
            get
            {
                return _lastKnownBalance;
            }
            set
            {
                _lastKnownBalance = value;
            }
        }
    }
}
