﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public abstract class State<T>
    {
        public abstract void OnExit(T owner);
        public virtual void Execute(T owner){}
        public abstract void OnEnter(T owner);
        public abstract void OnMessage(T owner, Message msg);        
    }
}
