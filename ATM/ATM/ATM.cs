using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CashMachine
{
    public class ATM : IRecipient
    {        
        private static uint[] _nominals = { 20, 50, 100, 200 };
        private static uint[] _defaultAmounts = { 20, 50, 100, 200, 500 };
        private Dictionary<uint, uint> _moneyAvailable;
        private const uint _limitWithdraw = 10000;
        private const uint _limitNotes = 99;
        private const uint _attemptsAllowedPIN = 3;

        private uint _id;
        private string _location;

        private Card _currentCard = null;

        private uint _attemptsPIN = 0;

        private Screen _screen = null;

        private State _previousState;
        private State _currentState;

        //Вспомогательная структура данных для алгоритма выдаачи денег
        private struct Dispension
        {
            public uint noteCount; //Оптимальное количество купюр
            public uint[] components; //Расклад купюр
        }

        public enum InputType
        {
            OK,
            CANCEL,
            MENU_LEDGER,
            MENU_WITHDRAW,
            MENU_TRANSFER,
        }

        public enum State
        {
            Maintenance,
            Idle,
            //Waiting,
            RequestPIN,
            MenuMain,
            MenuLedger,
            MenuWithdrawal,
            MenuWithdrawalResult,
            MenuTransfer
        }

        public ATM(uint number, string location)
        {
            _id = number;
            _location = location;

            _moneyAvailable = new Dictionary<uint, uint>();
            for (uint i = 0; i < _nominals.Length; i++)
            {
                _moneyAvailable.Add(_nominals[i], 0);
            }

            _currentState = State.Maintenance;
        }

        public const uint WITHDRAWAL_OK = 0;
        public const uint WITHDRAWAL_IMPOSSIBLE = 1;
        public const uint WITHDRAWAL_BALANCE_LOW = 2;
        public const uint WITHDRAWAL_NOT_MULT_TEN = 3;
        public const uint WITHDRAWAL_ZERO = 4;

        private long DispensingCash(uint sumToWithdraw)
        {
            //Наибольший общий делитель всех номиналов
            const uint smallestPart = 10;
            //Массив оптимумов для каждой возможной суммы
            Dispension[] dp = new Dispension[_limitWithdraw + 1];
            for (int i = 0; i < dp.Length; i++)
            {
                dp[i].components = new uint[_nominals.Length];
            }
            //База динамического программирования 
            dp[0].noteCount = 1;
            //Шаг динамического программирования
            for (uint indexNote = 0; indexNote < _nominals.Length; indexNote++) //Для каждого номинала
            {
                uint note = _nominals[indexNote]; //Текущий номинал
                uint noteCount = _moneyAvailable[note]; //Сколько купюр текущего номинала доступно
                for (uint i = 0; i < noteCount; i++) //Для каждой купюры отдельно
                {
                    //Проводим релаксацию всего массива
                    for (uint currentSum = _limitWithdraw; currentSum >= note; currentSum -= smallestPart)
                    {
                        Dispension previous = dp[currentSum - note];
                        if (previous.noteCount > 0)
                        if (dp[currentSum].noteCount == 0 || //Если расклада не существовало
                            previous.noteCount + 1 < dp[currentSum].noteCount) //Если новый расклад лучше старого
                        {
                            dp[currentSum].noteCount = previous.noteCount + 1; //Релаксация
                            for (int j = 0; j < _nominals.Length; j++)
                                dp[currentSum].components[j] = previous.components[j];
                            dp[currentSum].components[indexNote]++;
                        }
                    }
                }
            }
            if (dp[sumToWithdraw].noteCount == 0) //Если сумму выдать невозможно
            {
                long answer = 0;
                //Предлагаем пользователю альтернативные суммы
                long less = sumToWithdraw, more = sumToWithdraw;
                //Альтернатива слева
                while (less > 0 && dp[less].noteCount == 0)
                    less -= smallestPart;
                answer += less * 1000000;
                //Альтернатива справа
                while (more <= _limitWithdraw && dp[more].noteCount == 0)
                    more += smallestPart;
                if (more <= _limitWithdraw)
                    answer += more;
                return -answer;
            }
            else //Иначе выдаем оптимальный расклад купюр
            {
                //Шифруем оптимальный расклад в одно число
                long answer = 0;
                long factor = 1;
                for (int i = dp[sumToWithdraw].components.Length - 1; i >= 0; i--)
                {
                    answer += factor * dp[sumToWithdraw].components[i];
                    factor *= 1000;
                }
                return answer;
            }
        }

        private List<uint> _currentWithdrawResult = null;

        public void CheckWithdraw(uint amount)
        {
            MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.BALANCE, currentCard.cardNumber);
            List<uint> result = new List<uint>();
            if (amount == 0)
                result.Add(WITHDRAWAL_ZERO);
            else if (amount > AvailableBalance())
                result.Add(WITHDRAWAL_BALANCE_LOW);
            else if (amount % 10 != 0)
                result.Add(WITHDRAWAL_NOT_MULT_TEN);
            else
            {
                long cipher = DispensingCash(amount);
                if (cipher < 0)
                {
                    result.Add(WITHDRAWAL_IMPOSSIBLE);
                    long less = (-cipher) / 1000000;
                    if (less > 0)
                        result.Add((uint)less);
                    long more = (-cipher) % 1000000;
                    if (more > 0)
                        result.Add((uint)more);
                }
                else
                {
                    result.Add(WITHDRAWAL_OK);
                    result.Add(amount);
                    for (int i = _nominals.Length - 1; i >= 0; i--)
                    {
                        long number = cipher % 1000;
                        cipher /= 1000;
                        uint nominal = _nominals[i];
                        if (number > 0)
                        {
                            result.Add(nominal);
                            result.Add((uint)number);
                            _moneyAvailable[nominal] -= (uint)number;
                        }
                        //_currentCard.ReduceBalance((int)amount);
                    }
                }
            }
            _currentWithdrawResult = result;
            _currentState = State.MenuWithdrawal;
            if (result[0] == WITHDRAWAL_OK)
            {
                _screen.DrawLoading();
                MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.WITHDRAWAL, currentCard.cardNumber + ';' + amount.ToString());
            }
            else
            {
                _screen.DrawResultWithdrawal(result);
            }            
        }

        public void AddMoney(uint nominal, uint number)
        {
            if (_moneyAvailable.ContainsKey(nominal))
            {
                _moneyAvailable[nominal] += number;
                if (_moneyAvailable[nominal] > _limitNotes)
                    _moneyAvailable[nominal] = _limitNotes;
            }
        }

        public bool HasMoney()
        {
            uint sum = 0;
            for (int i = 0; i < _nominals.Length; i++)
            {
                sum += _moneyAvailable[_nominals[i]];
            }
            return (sum > 0);
        }

        public void HandleMessage(Message msg)
        {
            //switch (_currentState)
            //{
            //    //case State.Maintenance:
            //    //    if (msg.messageType == Message.Type.INPUT &&
            //    //        msg.content == ATM.InputType.OK.ToString())
            //    //    {
            //    //        _currentState = State.Idle;
            //    //    }
            //    //break;

            //    case State.Idle:

            //    break;

            //    case State.Waiting:
                    if(msg.content == Bank.BANK_CONNECTION_ERROR)
                    {
                        // TODO: Maintenance state notification should distinguish between absence of money and absence of Internet connection.
                        _currentState = State.Maintenance;
                        _screen.DrawMaintenance(/*TODO: Accept maintenance reason as a parameter*/);
                        return;
                    }
                    else if (msg.content == Bank.BANK_TRANSACTION_ERROR)
                    {
                        _currentState = State.Maintenance;
                        _screen.DrawMaintenance(/*TODO: Accept maintenance reason as a parameter*/);
                        return;
                    }
                    if (msg.messageType == Message.Type.VALIDATE_CARD)
                    {
                        if (msg.content == "INVALID" || msg.content == "BLOCKED")
                        {                            
                            _screen.DrawFaceControlFailed(msg.content);                            
                        }                        
                        else
                        {
                            //1234;0.00;0;0;0.00;0.00
                            // "PIN;credit_limit;commission_own;comission_credit;min_comission_own;min_comission_credit"
                            string cont = msg.content;
                            currentCard.expectedPIN = cont.Substring(0, cont.IndexOf(';'));
                            cont = cont.Remove(0, cont.IndexOf(';') + 1);
                            float f;
                            float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                            currentCard.creditLimit = f;
                            cont = cont.Remove(0, cont.IndexOf(';') + 1);
                            float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                            currentCard.comissionOwn = f;
                            cont = cont.Remove(0, cont.IndexOf(';') + 1);
                            float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                            currentCard.comissionCredit = f;
                            cont = cont.Remove(0, cont.IndexOf(';') + 1);
                            float.TryParse(cont.Substring(0, cont.IndexOf(';')), out f);
                            currentCard.minComissionOwn = f;
                            cont = cont.Remove(0, cont.IndexOf(';') + 1);
                            float.TryParse(cont, out f);
                            currentCard.minComissionCredit = f;
                            _currentState = State.RequestPIN;
                            _screen.DrawRequestPIN();
                        }
                    }
                    else if(msg.messageType == Message.Type.BALANCE)
                    {
                        currentCard.balance = float.Parse(msg.content);
                        //_currentState = State.MenuLedger;
                        //_screen.DrawLedgerMenu();
                        switch (_currentState)
                        {
                            case State.MenuLedger:
                                _screen.DrawLedgerMenu();
                                break;
                            case State.MenuWithdrawal:
                                _screen.DrawWithdrawal();
                                break;
                            case State.MenuTransfer:
                                //Do nothing: it's a last-minute balance check.
                                //_screen.DrawTransferMenu();
                                break;
                            default: break;
                                //throw new NotImplementedException();                                
                        }
                    }
                    else if (msg.messageType == Message.Type.WITHDRAWAL)
                    {
                        currentCard.balance = float.Parse(msg.content);
                        _currentState = State.MenuWithdrawalResult;
                        _screen.DrawResultWithdrawal(_currentWithdrawResult);
                    }
                    else if (msg.messageType == Message.Type.TRANSFER)
                    {
                        if (msg.content == "INVALID")
                        {
                            _screen.DrawTransferResult(TRANSFER_INVALID_RECIPIENT);
                            return;
                        }
                        _screen.DrawTransferResult(TRANSFER_OK);
                    }
            //    break;
            //}
        }

        public void Maintenance()
        {
            _currentState = State.Maintenance;
            _screen.DrawMaintenance();
        }

        public void ReturnToIdle()
        {
            if (!HasMoney())
            {
                Maintenance();
            }
            else
            {
                _currentState = State.Idle;
                _screen.DrawIdle();
            }
        }

        public void MainMenu()
        {
            _currentState = State.MenuMain;
            _screen.DrawMainMenu();
        }

        public void WithdrawalMenu()
        {
            _currentState = State.MenuWithdrawal;
            _screen.DrawWithdrawal();
        }
       
        public void LedgerMenu()
        {
            _currentState = State.MenuLedger;
            _screen.DrawLoading();
            MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.BALANCE, currentCard.cardNumber);
        }

        public void TransferMenu()
        {
            _currentState = State.MenuTransfer;
            _screen.DrawTransferMenu();
        }

        public void AcceptInput(InputType it, string input = "")
        {
            HandleMessage(new Message(this,this,Message.Type.INPUT,it.ToString() + (input.Length > 0 ? ':' + input:"")));
        }
        
        public void InsertCard(string number)
        {
            // TODO: check whether the card is consumed!!!
            _currentCard = new Card(number);
            _currentState = State.Idle;
            _screen.DrawLoading();
            MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.VALIDATE_CARD, currentCard.cardNumber);
        }

        public void BlockCard()
        {
            MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.BLOCK_CARD, currentCard.cardNumber);
            EjectCard();
        }

        public void EjectCard()
        {
            _currentCard = null;
            ReturnToIdle();
        }

        internal bool CheckPIN(string PIN)
        {
            if (PIN == currentCard.expectedPIN)
                return true;
            else
            {
                _attemptsPIN++;
                return false;
            }
        }

        internal void ResetPINAttempts()
        {
            _attemptsPIN = 0;
        }

        public float AvailableBalance()
        {            
            Card c = currentCard;
            // TODO: update the balance
            // wrong:
            //float r = c.balance - Math.Max(c.comissionOwn * c.balance, c.minComissionOwn) +
            //    c.creditLimit - Math.Max(c.comissionCredit * c.creditLimit, c.minComissionCredit);
            float res;
            if (c.comissionOwn * c.balance > c.minComissionOwn)
                res = c.balance / (1 + c.comissionOwn);
            else
                res = c.balance - c.minComissionOwn;
            if (c.comissionCredit * c.creditLimit > c.minComissionCredit)
                res += c.creditLimit / (1 + c.comissionCredit);
            else
                res += c.creditLimit - c.minComissionCredit;
            return res;
        }

        public const int TRANSFER_OK = 0;
        public const int TRANSFER_LOW_BALANCE = -1;
        public const int TRANSFER_INVALID_RECIPIENT = -2;

        public void CheckTransfer(string recipient, float amount)
        {
            //int result = 0;
            // Double check that balance is still satisfactory
             _screen.DrawLoading();
             MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.BALANCE, currentCard.cardNumber);
            if (amount > AvailableBalance())
            {
                //result = TRANSFER_LOW_BALANCE;
                _screen.DrawTransferResult(TRANSFER_LOW_BALANCE);
                return;
            }
            else
            {
                MessageDispatcher.instance.DispatchMessage(this, Bank.instance, Message.Type.TRANSFER, currentCard.cardNumber + ';' + recipient + ';' + amount.ToString());              
            }
        }

        public uint[] nominals
        {
            get
            {
                return _nominals;
            }
        }

        public uint[] defaultAmounts
        {
            get
            {
                return _defaultAmounts;
            }
        }

        public Dictionary<uint, uint> moneyAvailable
        {
            get
            {
                return _moneyAvailable;
            }
        }

        public uint limitNotes
        {
            get
            {
                return _limitNotes;
            }
        }

        public State currentState
        {
            get
            {
                return _currentState;
            }
        }

        public Card currentCard
        {
            get
            {
                return _currentCard;
            }
        }

        public uint attemptsPIN
        {
            get
            {
                return _attemptsPIN;
            }
        }

        public uint attemptsAllowedPIN
        {
            get
            {
                return _attemptsAllowedPIN;
            }
        }

        public Screen screen
        {
            get
            {
                return _screen;
            }
            set
            {
                _screen = value;
            }
        }
    }
}
