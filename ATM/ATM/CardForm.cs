﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CashMachine
{
    public partial class CardForm : Form
    {
        private ATM _master;
        
        public CardForm(ATM master, string text = "Enter your card number", int maxLength = 16)
        {
            InitializeComponent();
            _master = master;
            this.HintLabel.Text = text;
            this.NumBox.MaxLength = maxLength;
        }

        private void ProceedButton_Click(object sender, EventArgs e)
        {
            switch (HintLabel.Text)
            {
                case "Enter your card number":
                    _master.InsertCard(NumBox.Text);
                break;

                case "Enter the password to go to maintenance mode":
                    _master.Maintenance();
                break;

                case "Input the amount that is a multiple of 10":
                    uint amount;
                    uint.TryParse(NumBox.Text, out amount);
                    _master.CheckWithdraw(amount);
                break;
            }
            Close();
        }

        private void NumBox_TextChanged(object sender, EventArgs e)
        {
            switch (HintLabel.Text)
            {
                case "Enter your card number":
                    ProceedButton.Enabled = (NumBox.Text.Trim().Length == NumBox.MaxLength);
                    break;

                case "Enter the password to go to maintenance mode":
                    ProceedButton.Enabled = (Bank.instance.CheckPassword(NumBox.Text));
                    break;

                case "Input the amount that is a multiple of 10":
                    ProceedButton.Enabled = true;
                break;
            }
        }

        private void CardFrom_Closing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
        }

        private void OnlyDigits(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
    }
}
