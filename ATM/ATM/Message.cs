﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class Message
    {
        public enum Type
        {
            INPUT, 
            VALIDATE_CARD, 
            BALANCE, 
            WITHDRAWAL,
            NO_MONEY,
            BLOCK_CARD,
            TRANSFER
        };

        // This comparer is used in SortedSet<Message> collection to sort by dispatchTime.
        public class SetComparer : IComparer<Message>
        {            
            /// <summary>
            /// Compares x and y using dispatch time, then sender, then recipient.
            /// </summary>            
            /// <returns> 0 on equality, negative value when x #&lt; y and positive value otherwise.</returns>
            public int Compare(Message x, Message y)
            {                
                if (x == y) // Reference equality check
                    return 0;
                int dtCompRes = (int)(x.dispatchTime - y.dispatchTime).Ticks;   // Compare by dispatch time
                if (dtCompRes == 0)
                {
                    if (x.sender == y.sender && x.recipient == y.recipient)
                        return 0;   // Same dispatch time, same sender, same recipient
                    else
                        return -1;  // Sender or recepient differ; messages should not be considered equal
                }
                else
                    return dtCompRes;   // Compare by dispatch time
            }
        }

        Message.Type _messageType;
        string _content;
        IRecipient _sender;
        IRecipient _recipient;
        DateTime _dispatchTime;

        public const double DELAY_IMMEDIATE = -1.0;
        public const double DELAY_NEVER = double.PositiveInfinity;

        // .NET tick is equal to 100 nanoseconds, hence one second is equal to...
        private const double TICKS_PER_SECOND = 10000000.0;

        // IDEA: Maybe we could make Message a public subclass of MessageDispatcher and make its constructor private.

        /// <summary>
        /// Constructs a Message to be sent between IRecipient instances using MessageDispatcher.
        /// </summary>        
        /// <param name="delay">delay in seconds, also supports Message.DELAY_IMMEDIATE and Message.DELAY_NEVER</param>
        public Message(IRecipient sndr, IRecipient rcvr, Message.Type msgType, string cont, double delay = DELAY_IMMEDIATE)
        {
            _content = cont;
            _sender = sndr;
            _recipient = rcvr;
            _messageType = msgType;
            if (delay <= 0)
            {
                _dispatchTime = DateTime.Now;
            }
            else if (delay == DELAY_NEVER)
            {
                _dispatchTime = DateTime.Now + (new TimeSpan(long.MaxValue));
            }
            else
            {
                // Calculating number of 100-nanosecond units, equivalent to delay in seconds.
                long ticksDiff = (long)(delay * TICKS_PER_SECOND);                
                _dispatchTime = DateTime.Now + (new TimeSpan(ticksDiff));
            }
        }

        public string content
        {
            get
            {
                return _content;
            }
        }

        public IRecipient sender
        {
            get
            {
                return _sender;
            }
        }

        public IRecipient recipient
        {
            get
            {
                return _recipient;
            }
        }

        public DateTime dispatchTime
        {
            get
            {
                return _dispatchTime;
            }
        }

        public Message.Type messageType
        {
            get
            {
                return _messageType;
            }
        }
    }
}
