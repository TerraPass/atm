﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class MessageDispatcher
    {
        private static MessageDispatcher _instance;

        // Delayed messages set sorted by dispatch time; all members are unique
        private SortedSet<Message> _priorityQueue = new SortedSet<Message>(new Message.SetComparer());
        
        MessageDispatcher()
        {
            //TODO or not to do?
        }
        public static MessageDispatcher instance
        {
            get
            {
                if (_instance == null)
                    _instance = new MessageDispatcher();
                return _instance;
            }
        }

        void Discharge(Message msg)
        {
            msg.recipient.HandleMessage(msg);
        }

        /// <summary>
        /// Delivers the message msg to its recipient either immediately or after delay.
        /// </summary>
        /// <param name="msg">the message to deliver</param>
        public void DispatchMessage(Message msg)
        {
            // Immediate delivery
            if (msg.dispatchTime <= DateTime.Now)
            {
                Discharge(msg);
            }
            else
            {
                _priorityQueue.Add(msg);                
            }
        }

        /// <summary>
        /// Delivers the message to its recipient either immediately or after delay.
        /// </summary>        
        /// <param name="delay">delay in seconds, also supports Message.DELAY_IMMEDIATE and Message.DELAY_NEVER</param>
        public void DispatchMessage(IRecipient sndr, IRecipient rcvr, Message.Type msgType, string content, double delay = Message.DELAY_IMMEDIATE)
        {
            DispatchMessage(new Message(sndr, rcvr, msgType, content,delay));
        }

        /// <summary>
        /// Sends expired delayed messages, if any.
        /// </summary>
        public void Update()
        {
            while ((_priorityQueue.Count != 0) && (_priorityQueue.ElementAt(0).dispatchTime <= DateTime.Now))
            {
                Message curMsg = _priorityQueue.ElementAt(0);
                Discharge(curMsg);
                _priorityQueue.Remove(_priorityQueue.ElementAt(0));
            }
        }
    }
}
