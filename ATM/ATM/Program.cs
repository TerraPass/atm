﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace CashMachine
{
    static class Program
    {
        // Simulated ATM instance.
        public static ATM atm = null;

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            atm = new ATM(0, "Kyiv-Mohyla Academy");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Task t = Task.Factory.StartNew(() => {
            //    while (true)
            //    {
            //        atm.stateMachine.Update();
            //        MessageDispatcher.instance.Update();
            //        System.Threading.Thread.Sleep(3000);
            //    }
            //});            
            Application.Run(new Screen(atm));           
        }
    }
}
