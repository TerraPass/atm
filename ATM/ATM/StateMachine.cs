﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashMachine
{
    public class StateMachine<T>
    {
        private T _owner;
        private State<T> _previousState;
        private State<T> _currentState;

        public StateMachine(T owner, State<T> initialState)
        {
            _owner = owner;
            _previousState = null;
            _currentState = initialState;
            //initialState.OnEnter(_owner);
        }

        public void Update()
        {
            if (_currentState != null)
                _currentState.Execute(_owner);
        }

        public void ChangeState(State<T> newState)
        {
            _previousState = _currentState; //remember the current state
            _currentState.OnExit(_owner); //quit the current state
            _currentState = newState; //change the state
            _currentState.OnEnter(_owner); //start the new state
        }

        public void ReturnToPreviousState()
        {
            ChangeState(_previousState);
        }

        public State<T> previousState
        {
            get
            {
                return _previousState;
            }
            set
            {
                _previousState = value;
            }
        }

        public State<T> currentState
        {
            get
            {
                return _currentState;
            }
            set
            {
                _currentState = value;
            }
        }
    }
}
