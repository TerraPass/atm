--
-- MySQL 5.1.61
-- Mon, 17 Dec 2012 23:24:03 +0000
--

CREATE TABLE `account_types` (
   `account_type` smallint(5) unsigned not null,
   `credit_limit` decimal(10,2) unsigned,
   `comission_own` double unsigned default '0',
   `comission_credit` double unsigned default '0',
   `min_comission_own` decimal(10,2) unsigned default '0.00',
   `min_comission_credit` decimal(10,2) unsigned default '0.00',
   PRIMARY KEY (`account_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `account_types` (`account_type`, `credit_limit`, `comission_own`, `comission_credit`, `min_comission_own`, `min_comission_credit`) VALUES 
('0', '0.00', '0', '0', '0.00', '0.00'),
('1', '0.00', '0.01', '0', '1.00', '0.00'),
('2', '500.00', '0.01', '0.03', '1.00', '10.00');

CREATE TABLE `cards` (
   `card_number` char(16) not null,
   `pin` char(4) not null default '0000',
   `card_balance` decimal(10,2) default '0.00',
   `account_type` smallint(5) unsigned default '0',
   `is_blocked` bit(1),
   PRIMARY KEY (`card_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cards` (`card_number`, `pin`, `card_balance`, `account_type`, `is_blocked`) VALUES 
('9999888877770001', '0000', '999.99', '1', ''),
('1111222233330001', '0000', '540.00', '1', ''),
('1111222233330002', '0000', '300.00', '2', ''),
('9999888877770002', '0000', '-233.21', '2', ''),
('0000000000000001', '0000', '5000.00', '0', ''),
('1234567890123456', '1234', '100500.00', '0', '');